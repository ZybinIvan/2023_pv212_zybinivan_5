#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QFileDialog>
#include <QInputDialog>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <QMessageBox>

QString Qtext;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void getSim(const std::string &sub, const std::string &str, double k, double &s) {
    if (sub.size() < 5) {
        return;
    }
    if (str.find(sub) != std::string::npos) {
        s += k;
        return;
    } else {
        std::string buf(sub.begin(), sub.begin() + sub.size() / 2);
        getSim(buf, str, k / 2, s);
        buf = {sub.begin() + sub.size() / 2, sub.end()};
        getSim(buf, str, k / 2, s);
    }
}

void MainWindow::on_pushButton_clicked()
{
    std::ifstream selectionFS("C:/Users/Ivan/Documents/Laba10/selection.txt");

    std::stringstream buf1;

    std::string selection;
    std::string text = Qtext.toStdString();


    if (selectionFS) {
        buf1 << selectionFS.rdbuf();
        selection = buf1.str();
    } else {
        std::cerr << "error";
    }


    selection.erase(std::remove_if(selection.begin(), selection.end(),
                                   [](unsigned char c) { return std::isspace(c) || std::ispunct(c); }),
                    selection.end());
    text.erase(std::remove_if(text.begin(), text.end(),
                              [](unsigned char c) { return std::isspace(c) || std::ispunct(c); }), text.end());


    double similarity = 0;
    getSim(text, selection, 100, similarity);
    std::string p = std::to_string(100 - similarity);

    QMessageBox::about(this, "Результат", "Уникальность: " + QString::fromStdString(p) + "%");
}

void MainWindow::on_textEdit_textChanged()
{
    Qtext = ui->textEdit->toPlainText();
}
